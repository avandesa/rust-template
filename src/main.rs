use rust_template::foo;

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let a = foo();
    println!("It works! {}", a);
    Ok(())
}

fn main() {
    if let Err(err) = run() {
        eprintln!("Error: {}", err);

        ::std::process::exit(1);
    }
}
