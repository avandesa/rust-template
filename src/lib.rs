//! # rust-template
//!
//! Top-level documentation of the crate goes here. If the crate is a binary, this can probably be
//! ignored.

mod example;
pub use example::foo;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
