# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project
adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2019-11-19

### Added

- List of additions for this release

### Changed

- List of API changes for this release

### Deprecated

- List of deprecated items for this release

### Removed

- List of removed items for this release

<!--TODO: Update these links-->
[unreleased]: https://gitlab.com/avandesa/rust-template/v0.1.0...HEAD
[0.1.0]: https://gitlab.com/avandesa/rust-template/v0.1.9...v0.1.0

[keep]: https://keepachangelog.com/en/1.0.0
[semver]: https://semver.org/spec/v2.0.0.html
