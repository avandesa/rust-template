# TODO

Things to change:

1. In [`Cargo.toml`](Cargo.toml), update
    * Crate name
    * Crate version
    * Description
    * Repository link
    * Keywords
    * Categories
    * CI Badge link
    * Dependencies
2. Implement modules as needed
    * Import them in [`lib.rs`](src/lib.rs)
    * If the tests aren't super long, use a submodule in the same file. Otherwise, create a
      subfolder for the module with a spearate tests files. Either way, keep the test local to the
      modules.
3. If this is a binary, implement `run` in [`main.rs`](src/main.rs)
4. Update the [Readme](README.md).
    * Change the shields.io links
    * Change the badge links
    * Add a real example
5. `cargo publish`
6. Update the [changelog](CHANGELOG.md) as needed
