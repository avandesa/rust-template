# Rust crate template

<!--TODO: Change these:-->

[![Crates.io](https://img.shields.io/crates/v/CHANGEME)][cratesio]
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/avandesa/rust-template)][pipeline]

[Changelog](CHANGELOG.md)

This is a template for a new library or binary crate. It includes:

* a `README` (duh)
* a [`Cargo.toml`](Cargo.toml),
* the [MIT License](LICENSE.md),
* a [configuration for GitLab CI](.gitlab-ci.yml)
* a [`.rustfmt.toml`](.rustfmt.toml)
* a basic project structure, with a sample module, lib, and main.

See [`TODO.md`](TODO.md) for a list of things to change before publishing a project. Or
alternatively, just `rg TODO`.

## Usage

`Cargo.toml`:

```toml
[dependencies]
CHANGEME = "0.1.0"
```

Source

```rust
use rust-template::foo;

assert_eq!(foo(), 1);
```

## Versioning

This project adheres to [Semantic Versioning][semver].

The minimum supported `Rust` version is `1.39.0`. An increase in the minimum compiler version is considered a breaking change.

<!--TODO: Change these:-->

[cratesio]: https://crates.io/crates/CHANGEME
[pipeline]: https://gitlab.com/avandesa/rust-template/pipelines

[semver]: https://semver.org/spec/v2.0.0.html